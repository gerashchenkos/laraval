@extends('layouts.app')

@section('content')
    <main role="main" class="inner cover mt-5">
        <div class="container">
            <div class="row">
                <h3>Thank you for your order. Your order id is {{$order->id}}</h3>
            </div>
        </div>
    </main>
@endsection
