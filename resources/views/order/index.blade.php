@extends('layouts.app')

@section('content')
    <main role="main" class="inner cover mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4>Your order Details</h4>
                    <div class="row">
                        Shipping Address: {{ $address->receiver_name .", " . $address->address .", " . $address->city .", " . $address->state .", " . $address->zip}}
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            Product
                        </div>
                        <div class="col-md-3">
                            Quantity
                        </div>
                        <div class="col-md-3">
                            Total
                        </div>
                    </div>
                    @php($total = 0)
                    @foreach($products as $product)
                        <div class="row">
                            <div class="col-md-6">
                                {{$product['product_title']}}
                            </div>
                            <div class="col-md-3">
                                {{$product['quantity']}}
                            </div>
                            <div class="col-md-3">
                                {{$product['price'] * Helper::getCurrencyRate() * $product['quantity']}}
                            </div>
                        </div>
                        @php($total = $total + $product['price'] * Helper::getCurrencyRate() * $product['quantity'])
                    @endforeach
                    <div class="row justify-content-end">
                        <div class="col-md-3">
                            <b>{{$total}}</b>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="creditCardForm">
                        <div class="heading">
                            <h4>Confirm Purchase</h4>
                        </div>
                        <div class="payment">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form method="POST" action="{{ route('pay') }}">
                                @csrf
                                <div class="form-group CVV">
                                    <label for="cvv">CVV</label>
                                    <input type="text" name="cvv" class="form-control" required id="cvv">
                                </div>
                                <div class="form-group" id="card-number-field">
                                    <label for="cardNumber">Card Number</label>
                                    <input type="text" name="card_number" required  class="form-control" id="cardNumber">
                                </div>
                                <div class="form-group" id="expiration-date">
                                    <label>Expiration Date</label>
                                    <select class="form-control" name="exp_month">
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="exp_year">
                                            @for($i = 2019; $i < 2039; $i++)
                                            <option value="{{$i}}"> {{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                <div class="form-group" id="credit_cards">
                                </div>
                                <div class="form-group" id="pay-now">
                                    <input type="hidden" name="amount" value="{{$total}}">
                                    <button type="submit" class="btn btn-primary" id="confirm-purchase">Confirm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
