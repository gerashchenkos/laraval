<div id="tree">
    @foreach (Helper::getCategoriesTree() as $r)
        {!! $r !!}
    @endforeach
</div>
<script>
    $(function () {
        $("ul li:not(:last-child) a").removeAttr('href');
    });
</script>