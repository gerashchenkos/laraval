<div class="col-sm-6">
    <div class="card mb-4">
        <div><a href="/product/{{ $product['id'] }}">
                <img class="card-img-top"
                     src="https://coactivehealthshop.co.uk/wp-content/uploads/test-product.png">
            </a>
        </div>
        <div class="card-body">
            <p class="card-text">{{ $product['title'] }}</p>
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <form method="POST" action="{{ route('cart.store') }}">
                        @csrf
                        <input type="hidden" name="product_id"
                               value="{{ $product['id'] }}">
                        <button type="submit" class="btn btn-sm btn-outline-secondary">
                            Order
                        </button>
                    </form>
                </div>
                <small class="text-muted">{{ $product['price'] * Helper::getCurrencyRate() }} UAH</small>
            </div>
        </div>
    </div>
</div>
