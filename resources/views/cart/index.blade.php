@extends('layouts.app')

@section('content')
    <main role="main" class="inner cover mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    Product
                </div>
                <div class="col-md-2">
                    Price per unit
                </div>
                <div class="col-md-2">
                    Quantity
                </div>
                <div class="col-md-2">
                    Total
                </div>
            </div>
            @php($total = 0)
            @foreach($products as $product)
                <div class="row">
                    <div class="col-md-6">
                        <a href="/product/{{ $product['product_id'] }}">{{$product['product_title']}}
                            ({{$product['category_title']}})</a>
                    </div>
                    <div class="col-md-2">
                        {{$product['price'] * Helper::getCurrencyRate()}}
                    </div>
                    <div class="col-md-2">
                        {{$product['quantity']}}
                    </div>
                    <div class="col-md-2">
                        {{$product['price'] * Helper::getCurrencyRate() * $product['quantity']}}
                    </div>
                </div>
                @php($total = $total + $product['price'] * Helper::getCurrencyRate() * $product['quantity'])
            @endforeach
            <div class="row">
                <div class="col-md-10">

                </div>
                <div class="col-md-2">
                    <b>{{$total}}</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a class="btn-danger btn" href="{{$previous}}">Return</a>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-2">
                    <a class="btn btn-info" href="{{ route('cart.show.shipping') }}">Continue</a>
                </div>
            </div>
        </div>
    </main>
@endsection
