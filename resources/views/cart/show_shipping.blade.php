@extends('layouts.app')

@section('content')
    <main role="main" class="inner cover mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>Please select existing address ...</h3>
                    <form method="GET" action="{{route('order')}}">
                        @csrf
                        @foreach($addresses as $address)
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <input type="radio" name="shipping_id" aria-label="{{ $address->receiver_name .", " . $address->address .", " . $address->city .", " . $address->state .", " . $address->zip}}" value="{{ $address->id }}">
                                    </div>
                                </div>
                                <input type="text" class="form-control" disabled value="{{ $address->receiver_name .", " . $address->address .", " . $address->city .", " . $address->state .", " . $address->zip}}">
                            </div>
                        @endforeach
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-outline-info" value="Continue With Selected">
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <h3>... or add new address</h3>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{ route('cart.store.shipping') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" required class="form-control" value="{{ old('receiver_name') }}" name="receiver_name" id="receiver_name" aria-describedby="" placeholder="Full Receiver Name">
                        </div>
                        <div class="form-group">
                            <input type="text" required class="form-control" value="{{ old('address') }}" name="address" id="address" aria-describedby="" placeholder="Address">
                        </div>
                        <div class="form-group">
                            <input type="text" required class="form-control" value="{{ old('city') }}" name="city" aria-describedby="" placeholder="City">
                        </div>
                        <div class="form-group">
                            <select name="state" class="form-control" required>
                                <option value="">Select State</option>
                                <option value="NY">New York</option>
                                <option value="AL">Alabama</option>
                                <option value="CA">California</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{ old('zip') }}" name="zip" aria-describedby="" placeholder="Zip Code">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-outline-info" value="Add new">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection
