<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cover.css') }}" rel="stylesheet">
</head>
<body class="text-center">
<div id="app" class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
        <div class="inner">
            <h3 class="masthead-brand">Shop</h3>
            <nav class="nav nav-masthead justify-content-center">
                <a class="nav-link {{ (request()->is('products*')) ? 'active' : '' }}" href="{{route('products')}}">Products</a>
                <a class="nav-link {{ (request()->is('cart')) ? 'active' : '' }}" href="{{route('cart')}}">Cart</a>
                @guest
                    <a class="nav-link {{ (request()->is('login')) ? 'active' : '' }}"
                       href="{{ route('login') }}">{{ __('Login') }}</a>
                    @if (Route::has('register'))
                        <a class="nav-link {{ (request()->is('register')) ? 'active' : '' }}"
                           href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                @else
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endguest
            </nav>
        </div>
    </header>
    <main class="py-4">
        @yield('content')
    </main>
    <footer class="mastfoot mt-auto">
        <div class="inner">
            <p>Our Test SHOP by Backend 2019</a>.</p>
        </div>
    </footer>
</div>
</body>
</html>
