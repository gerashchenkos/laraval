@extends('layouts.app')

@section('content')
    <main role="main" class="inner cover mt-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    @include('partials.category_tree')
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        @foreach($products as $product)
                            @include('partials.product')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
