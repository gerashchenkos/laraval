<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products/{slug?}', 'ProductController@index')->name('products');
Route::get('/product/{id}', 'ProductController@show')->name('product.show')->where('id', '[0-9]+');
Route::redirect('/', '/products');

Route::middleware(['auth'])->group(function () {
    Route::prefix('cart')->group(function () {
        Route::get('/', 'CartController@index')->name('cart');
        Route::post('store', 'CartController@store')->name('cart.store');
        Route::post('store_shipping', 'CartController@storeShipping')->name('cart.store.shipping');
        Route::get('show_shipping', 'CartController@showShipping')->name('cart.show.shipping');
    });
    Route::prefix('order')->group(function () {
        Route::get('/', 'OrderController@index')->name('order');
        Route::post('/', 'OrderController@pay')->name('pay');
    });
    Route::get('/admin', 'AdminController@index')->name('admin')->middleware(['check.admin']);
});
