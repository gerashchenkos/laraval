<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = ['product_id', 'user_id', 'quantity'];

    public static function store($productId, $userId)
    {
        $cart = Cart::firstOrNew(['product_id' => $productId, 'user_id' => $userId]);
        $cart->quantity = $cart->quantity + 1;
        $cart->save();
    }

    public static function listProducts($userId)
    {
        $products = Cart::where("user_id", $userId)
            ->join('products', 'products.id', '=', 'cart.product_id')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->select(
                'cart.*',
                'products.title as product_title',
                'products.price',
                'categories.title as category_title'
            )
            ->get();
        return $products;
    }

    public static function getProductsForOrder($userId)
    {
        $products = Cart::where("user_id", $userId)
            ->join('products', 'products.id', '=', 'cart.product_id')
            ->select(
                'products.id',
                'products.price',
                'cart.quantity'
            )
            ->get();
        return $products;
    }
}
