<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    protected $with = ['children'];

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id','id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }



    public static function fetchCategoryTreeList($parent = 0, $userTreeArray = '')
    {
        if (!is_array($userTreeArray)){
            $userTreeArray = [];
        }
        $result = Category::where('parent_id', $parent)->orderBy('id', 'asc')->get();
        if (count($result) > 0)
        {
            $userTreeArray[] = "<ul class='category-tree'>";
            foreach($result as $row){
                $userTreeArray[] = "<li><a href='/products/".$row['slug']."'>". $row["title"]."</a></li>";
                $userTreeArray = Category::fetchCategoryTreeList($row["id"], $userTreeArray);
            }
            $userTreeArray[] = "</ul><br/>";
        }
        return $userTreeArray;
    }
}
