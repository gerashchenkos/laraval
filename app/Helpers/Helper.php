<?php

namespace App\Helpers;

use App\Category;
use App\CurrencyRate;

class Helper
{
    public static function getCategoriesTree()
    {
        return Category::fetchCategoryTreeList();
    }

    public static function getCurrencyRate()
    {
        return CurrencyRate::orderBy('created_at', 'desc')->first()->uah_rate;
    }
}
