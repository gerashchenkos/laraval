<?php

namespace App\Libs;

use App\Category;
use App\CurrencyRate;
use App\User;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class StripeLib
{
    private const CURRENCY = 'UAH';
    private $stripe;

    public function __construct()
    {
        $this->stripe = Stripe::make(env('STRIPE_API_SECRET_KEY'));
    }

    public function pay(User $user, $data = [])
    {
        try {
            if (empty($user->stripe_customer_id)) {
                //add customer
                $customer = $this->stripe->customers()->create(
                    [
                        'email' => $user->email,
                        'name' => $user->name
                    ]
                );
                $user->stripe_customer_id = $customer['id'];
                $user->save();
            }
            $token = $this->stripe->tokens()->create(
                [
                    'card' => [
                        'number' => $data['card_number'],
                        'exp_month' => $data['exp_month'],
                        'cvc' => $data['cvv'],
                        'exp_year' => $data['exp_year']
                    ],
                ]
            );
            $card = $this->stripe->cards()->create($user->stripe_customer_id, $token['id']);
            $customer = $this->stripe->customers()->update(
                $user->stripe_customer_id,
                [
                    'default_source' => $card['id'],
                ]
            );
            //charge
            $charge = $this->stripe->charges()->create(
                [
                    'customer' => $customer['id'],
                    'currency' => self::CURRENCY,
                    'amount' => $data['amount'],
                ]
            );
        } catch (\Exception $e) {
            $message = $e->getMessage();
            return ["status" => "error", "error" => $message];
        }
        return ["status" => "ok", "data" => $charge];
    }
}
