<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request, $slug = null)
    {
        $products = Product::getProductsByCategory($slug);
        return view('product.index', ["products" => $products]);
    }

    public function show($id)
    {
        $product = Product::find($id);
        return view('product.show', ["product" => $product]);
    }
}
