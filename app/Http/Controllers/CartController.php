<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\ShippingAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function index()
    {
        $previousPage = Session::get('previous') ?? '/products';
        $products = Cart::listProducts(Auth::id());
        return view('cart.index', ["products" => $products, "previous" => $previousPage]);
    }

    public function store(Request $request)
    {
        $previousPage = URL::previous();
        Session::put('previous', $previousPage);
        $request->validate(
            [
                'product_id' => 'required|numeric|exists:products,id',
            ]
        );
        Cart::store($request->input('product_id'), Auth::id());
        return redirect()->route('cart');
    }

    public function showShipping()
    {
        $addresses = ShippingAddress::where("user_id", Auth::id())->get();
        return view('cart.show_shipping', ["addresses" => $addresses]);
    }

    public function storeShipping(Request $request)
    {
        $request->validate(
            [
                'receiver_name' => 'required|max:255',
                'address' => 'required|max:255',
                'city' => 'required|max:255',
                'zip' => 'required|digits_between:5,6',
                'state' => 'required|alpha|size:2',
            ]
        );
        $params = $request->all();
        $params['user_id'] = Auth::id();
        ShippingAddress::create($params);
        return back();
    }
}
