<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\ShippingAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Libs\StripeLib;
use App\Order;
use App\PaymentsLog;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'shipping_id' => 'required|exists:shipping_addresses,id'
            ]
        );
        if ($validator->fails()) {
            return redirect()->route('cart.show.shipping');
        }
        $shipping = ShippingAddress::find($request->input('shipping_id'));
        $products = Cart::listProducts(Auth::id());
        return view('order.index', ["address" => $shipping, "products" => $products]);
    }

    public function pay(Request $request)
    {
        $request->validate(
            [
                'cvv' => 'required|numeric|digits:3',
                'card_number' => 'required|numeric|digits:16',
                'exp_month' => 'required|digits:2|between:1,12',
                'exp_year' => 'required|digits:4',
            ]
        );
        $stripe = new StripeLib();
        $charge = $stripe->pay(Auth::user(), $request->all());
        if ($charge['status'] === "ok") {
            //save order into db
            $orderData = [];
            $orderData["user_id"] = Auth::id();
            $orderData["total_price"] = $charge['data']['amount'] / 100;
            $orderData["status"] = "processed";
            $orderData["products"] = Cart::getProductsForOrder(Auth::id());
            $order = Order::create($orderData);
            PaymentsLog::create(["order_id" => $order->id, "stripe_response" => json_encode($charge)]);
            //delete products from cart
            Cart::where('user_id', Auth::id())->delete();
            //show order than you page
            return view('order.pay', ["order" => $order]);
        } else {
            PaymentsLog::create(["stripe_response" => json_encode($charge)]);
            return redirect()->back()->withErrors([$charge['error']]);
        }
    }
}
