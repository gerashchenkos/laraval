<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentsLog extends Model
{
    protected $table = 'payments_log';
    protected $fillable = ['order_id', 'stripe_response'];
}
