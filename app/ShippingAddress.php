<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    protected $table = 'shipping_addresses';

    protected $fillable = ['user_id', 'receiver_name', 'address', 'city', 'state', 'zip'];
}
