<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CurrencyRate;

class GetCurrencyRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // set API Endpoint and API key
        $endpoint = 'latest';
        $access_key = env('FIXER_API_KEY');
        // Initialize CURL:
        $ch = curl_init('http://data.fixer.io/api/' . $endpoint . '?access_key=' . $access_key . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);
        // Decode JSON response:
        $exchangeRates = json_decode($json, true);
        // Access the exchange rate values, e.g. GBP:
        $rate = new CurrencyRate();
        $rate->uah_rate = round($exchangeRates['rates']['UAH'], 2);
        $rate->save();
    }
}
