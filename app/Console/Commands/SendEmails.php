<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is test email sending';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $email = $this->argument('email');
        Mail::raw('Текст письма', function($message) use ($email)
        {
            $message->from('us@example.com', 'Laravel');

            $message->to($email);
        });
    }
}
