<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /*protected $fillable = ['title', 'price', 'category_id', 'quantity'];*/

    protected $guarded =[];

    public static function getProductsByCategory($slug, $limit = 4)
    {
        if(!empty($slug)){
            $categoryId = Category::where("slug", $slug)->first()->id;
            $products = Product::where("category_id", $categoryId)->inRandomOrder()->limit($limit)->get();
        } else{
            $products = Product::inRandomOrder()->limit($limit)->get();
        }
        return $products;
    }

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }
}
